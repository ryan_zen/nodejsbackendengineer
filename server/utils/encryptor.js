'use strict';
var crypto = require('crypto')


const algorithm = 'aes-256-ctr'
const secret = process.env.ENCRYPTOR_SECRET || 'secretkey'

const encrypt = (msg) => {

  var cipher = crypto.createCipher(algorithm,secret)
  var crypted = cipher.update(msg,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;

}

const decrypt = (encrypted) => {
  var decipher = crypto.createDecipher(algorithm,secret)
  var dec = decipher.update(encrypted,'hex','utf8')
  dec += decipher.final('utf8');
  return dec;
}

const randomKeys = (char) => {

    let keys = '';

    const possibleCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
      'abcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < char; i++) {
      keys += possibleCharacters.charAt(
        Math.floor(Math.random() * possibleCharacters.length)
      );
    }
    return keys;
}

module.exports = {
  algorithm: algorithm,
  encrypt: encrypt,
  decrypt: decrypt,
  randomKeys: randomKeys
}