const bcrypt = require('bcrypt');
const crypto = require('crypto');
const nodemailer = require('nodemailer')
const encryptor = require('../utils/encryptor')

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
    },
    username: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    verification: {
      type: DataTypes.STRING,
      allowNull: true,
    },

  });

  // set up the associations so we can make queries that include
  // the related objects
  User.associate = function ({ AuthToken }) {
    User.hasMany(AuthToken);
  };

  // This is a class method, it is not called on an individual
  // user object, but rather the class as a whole.
  // e.g. User.authenticate('user1', 'password1234')
  User.authenticate = async function(username, password) {

    const user = await User.findOne({ where: { username } });
    const hash = await crypto.createHash('md5').update(password).digest("hex");

    if (hash == user.password) {
      return user
    }
    throw new Error('invalid password');
  }

  // in order to define an instance method, we have to access
  // the User model prototype. This can be found in the
  // sequelize documentation
  User.prototype.authorize = async function () {
    const { AuthToken } = sequelize.models;
    const user = this

    // create a new auth token associated to 'this' user
    // by calling the AuthToken class method we created earlier
    // and passing it the user id
    const authToken = await AuthToken.generate(this.id);

    // addAuthToken is a generated method provided by
    // sequelize which is made for any 'hasMany' relationships
    await user.addAuthToken(authToken);

    return { user, authToken }
  };


  User.prototype.logout = async function (token) {

    // destroy the auth token record that matches the passed token
    sequelize.models.AuthToken.destroy({ where: { token } });
  };

  User.prototype.sendVerification = async function (host, verCode) {
    try {
      const user = this
      var smtpTransport = nodemailer.createTransport({
          service: process.env.MAIL_SERVICE,
          auth: {
              user: process.env.MAIL_USER,
              pass: process.env.MAIL_PASSWORD
          }
      });

      let link = "http://"+host+"/verify?id="+verCode;
      let mailOptions = {
        to : user.email,
        subject : "Please confirm your Email account",
        html : "Hello,<br> Please Click on the link to verify your email.<br><a href="+link+">Click here to verify</a>" 
      }
      
      let result =  await smtpTransport.sendMail(mailOptions)

      console.log("Message sent: " + result.messageId);
      return true


    } catch (err) {
      throw new Error('Failed to send email');
    }


  }

  return User;
};