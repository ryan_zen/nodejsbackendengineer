'use strict';
var encryptor = require('../utils/encryptor')

module.exports = (sequelize, DataTypes) => {
  const AuthToken = sequelize.define('AuthToken', {
    token: DataTypes.STRING
  }, {});
  AuthToken.associate = function({ User }) {
    // associations can be defined here
    AuthToken.belongsTo(User);
  };

  // generates a random 15 character token and
  // associates it with a user
  AuthToken.generate = async function(UserId) {
    if (!UserId) {
      throw new Error('AuthToken requires a user ID')
    }

    let token = encryptor.randomKeys(16)

    return AuthToken.create({ token, UserId })
  }

  return AuthToken;
};