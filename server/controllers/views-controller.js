const express  = require('express');
const router = express.Router();

router.get('/', (req, res) => res.render('home', { user: req.user }));

router.get('/register', (req, res) => res.render('home', { user: req.user }));

router.get('/verification-sent', (req, res) => res.render('verification-sent', { user: req.user }));

module.exports = router;