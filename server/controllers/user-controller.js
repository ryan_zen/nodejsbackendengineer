const express = require('express');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const encryptor = require('../utils/encryptor')
const validator = require('../utils/validator')

const router = express.Router();

let frontSalt = process.env.PWD_FRONT_SALT || '$$'
let backSalt = process.env.PWD_BACK_SALT || '!!'


// grab the User model from the models folder, the sequelize
// index.js file takes care of the exporting for us and the
// syntax below is called destructuring, its an es6 feature
const { User } = require('../models');

router.get('/test', (req, res) => {
  let str = encryptor.encrypt('Helloworld')
  res.send(encryptor.decrypt(str));
  // res.send('Hello World!')
})

/* Register Route
========================================================= */
router.post('/register', async (req, res) => {

  let salted = frontSalt + req.body.password + backSalt

  // hash the password provided by the user with sha256 so that
  // we are never storing plain text passwords. This is crucial
  // for keeping your db clean of sensitive data
  const hash = crypto.createHash('md5').update(salted).digest("hex");

  try {

    // validate request
    if (!req.body.username || !req.body.email || !req.body.password) {
      return res.status(400).send({err: '002', msg: 'invalid form'});
    }

    // check user exist
    let found = await User.findAll({
      where: {
        username: req.body.username
      }
    })
    if (!found) {
      return res.status(400).send({err: '001', msg: 'username exist'});
    }


    // validate data
    if (req.body.username.length < 6 || req.body.username.length > 20) {
      return res.status(400).send({err: '001', msg: 'Username must be between 6 to 20 characters'});
    }

    if (req.body.password.length < 6 || req.body.password.length > 20) {
      return res.status(400).send({err: '001', msg: 'Password must be between 6 to 20 characters'});
    }

    if (validator.email(req.body.email) == false) {
      return res.status(400).send({err: '001', msg: 'Invalid email address'});
    }

    /** create verification url, create user with verification code */
    let key = encryptor.randomKeys(16)
    let verString = JSON.stringify({
      username: req.body.username,
      code: key
    })
    console.log('verString',verString)
    let verCode = encryptor.encrypt(verString)

    // create a new user with the password hash from bcrypt
    let user = await User.create(
      Object.assign(req.body, {
        password: hash,
        verification: key
      })
    );

    // data will be an object with the user and it's authToken
    // let data = await user.authorize();
    let result = await user.sendVerification(req.get('host'), verCode)
    if (!result) {
      throw new Error('Failed to send email');
    }

    // send back the new user and auth token to the
    // client { user, authToken }
    return res.json({user})

  } catch(err) {
    console.log(err)
    return res.status(400).send({err: '001', msg: 'failed to send verfication email'});
  }

});

router.get('/verify', async(req, res) => {

  try {

    let code = req.query.id 
    if (!code) {
      res.render('invalid-verification', { user: req.user })
      return
    }
    console.log(code)
    console.log(encryptor.decrypt(code))
    let data = JSON.parse(encryptor.decrypt(code))

    if (!data.code || !data.username) {
      res.render('invalid-verification', { user: req.user })
      return 
    }

    let user = await User.findOne({
      username: data.username
    })

    if (user.verification == '') {
      res.render('already-verified', { user: req.user })
      return
    }

    if (data.code !== user.verification) {
      res.render('invalid-verification', { user: req.user })
      return 
    }

    user.verification = '';
    user.save();

    let result = await user.authorize();

    res.redirect('/')


  } catch(err) {
    // console.log(err)
    return res.status(400).send(
      { errors: [{ message: 'unable to verify' }] }
    );
  }

})

/* Login Route
========================================================= */
router.post('/login', async (req, res) => {
  const { username, password } = req.body;

  // if the username / password is missing, we use status code 400
  // indicating a bad request was made and send back a message
  if (!username || !password) {
    return res.status(400).send(
      'Request missing username or password param'
    );
  }

  try {
    let salted = frontSalt + password + backSalt
    let user = await User.authenticate(username, salted)

    user = await user.authorize();
    return res.json(user);

  } catch (err) {
    return res.status(400).send('invalid username or password');
  }

});

/* Logout Route
========================================================= */
router.delete('/logout', async (req, res) => {

  // because the logout request needs to be send with
  // authorization we should have access to the user
  // on the req object, so we will try to find it and
  // call the model method logout
  const { user, cookies: { auth_token: authToken } } = req

  // we only want to attempt a logout if the user is
  // present in the req object, meaning it already
  // passed the authentication middleware. There is no reason
  // the authToken should be missing at this point, check anyway
  if (user && authToken) {
    await req.user.logout(authToken);
    return res.json({msg: 'Logged out.'})
  }

  // if the user missing, the user is not logged in, hence we
  // use status code 400 indicating a bad request was made
  // and send back a message
  return res.status(400).send(
    { errors: [{ message: 'not authenticated' }] }
  );
});

/* Me Route - get the currently logged in user
========================================================= */
router.get('/me', (req, res) => {
  if (req.user) {
    return res.send(req.user);
  }
  res.status(404).send(
    { errors: [{ message: 'missing auth token' }] }
  );
});

router.delete('/delete_account', async (req, res) => {

  try {

    const { user } = req
    const authToken = req.cookies.auth_token


    if (!user || ! authToken) {
      return res.status(400).send({err: '001', msg: 'unable to delete account'});
    }

    await user.logout(authToken);
    await user.destroy()
    return res.json({msg: 'Account has been deleted.'})

  } catch (err) {
    console.log(err)
    return res.status(400).send({err: '001', msg: 'unable to delete account'});
  }

})

// export the router so we can pass the routes to our server
module.exports = router;