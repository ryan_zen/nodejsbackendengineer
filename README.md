# NodeJs Backend Engineer

Node Js Backend Engineer Test 
From : Indorse.io
Date : 2018 - 11 - 13


# Credits:
 - express framework boiler plate for user authentication
   - ref: https://medium.com/@jgrisafe/custom-user-authentication-with-express-sequelize-and-bcrypt-667c4c0edef5


# Basic feature!

  - The user can sign up as a new user. They are expected to see an email in their inbox.
  - The user can click the link in the email to verify the account.
  - The user can log in only after their email address is verified.
  - The user cannot create another account with the same email from an existing account.
  - A hacker if committing Man-in-the-middle attacks cannot know the plain-text password.
    The database owner (You) cannot store plaintext password or SHA256-hashed password in your DB.
  - Fields in the input object must be checked if they are required and if they have the right data types.
  - Full installation instruction must be available via readme.md. Indorse validators need to read these to run and test your   code.



### Installation

This app requires:
   - npm v6.4.1 
   - [Node.js](https://nodejs.org/) v10.30+.
   - sequelize-cli
   - nodemon - to restart app automatically when files is changed.


Install npm global packages
```sh
# install node
$ npm install -g node@10.13.0

# install sequelize
$ npm install -g sequelize-cli@5.3.0

# install nodemon
$ npm install -g nodemon@1.18.6
```

Create database in you mysql server, change it according to your .env file
for this exampe "indorse_main" is used.
```sh
CREATE [indorse_main];
```

Running the app
> By default the app will run PORT 3000
> This app also send email for verifications, so gmail account and credentials is required to be set-up in .env file


#### Building for source

In your project folder
```sh
# to install neccesary npm modules, you can check in package.json
$ npm install
```

Make sure the information in .env file correct. 
In general practice .env file should not be committed in .git, however i have done it so 
here just for documentation purposes. In real production environment, do not put .env file in git

```sh
# app environment production/dev
APP_ENV=development

# port the app will run on
APP_PORT=3000

# add additional character to password
PWD_FRONT_SALT= '$$'
PWD_BACK_SALT= '!!'

# database connections
DB_USERNAME=root
DB_PASSWORD=root
DB_DATABASE=indorse_main
DB_HOST=127.0.0.1
DB_DIALECT=mysql

# a feature that will wipe out and recreate database. by default false
DB_WIPEOUTDB=false

# email credential to send verification email
MAIL_SERVICE=Gmail
MAIL_USER=[YOUR EMAIL]
MAIL_PASSWORD=[YOUR PASSWORD]

# secret key for encryption
ENCRYPTOR_SECRET=secretkey1
```

Run the app
```sh
$ npm run dev
```

Open the browser and point it to http://localhost:3000/

